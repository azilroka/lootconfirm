local AddOnName = ...
local LC = LibStub('AceAddon-3.0'):NewAddon('LootConfirm', 'AceEvent-3.0')
local EP = LibStub('LibElvUIPlugin-1.0')
local E

function LC:GetOptions()
	if IsAddOnLoaded('ElvUI') and E.Options.args.general ~= nil then
		E.Options.args.general.args.general.args.autoRoll.disabled = function() return true end
	end
	local Ace3OptionsPanel = IsAddOnLoaded("ElvUI") and ElvUI[1] or Enhanced_Config[1]
	Ace3OptionsPanel.Options.args.lootconfirm = {
		type = 'group',
		name = select(2, GetAddOnInfo(AddOnName)),
		order = 3,
		args = {
			header = {
				order = 1,
				type = 'header',
				name = 'Automatic loot confirmation and auto greed/disenchant',
			},
			general = {
				order = 2,
				type = 'group',
				name = 'General',
				guiInline = true,
				get = function(info) return LootConfirmOptions[info[#info]] end,
    			set = function(info, value) LootConfirmOptions[info[#info]] = value end, 
				args = {
					Confirm = {
						order = 1,
						type = 'toggle',
						name = 'Auto Confirm',
						desc = 'Automatically click OK on BOP items',
					},
					Greed = {
						order = 2,
						type = 'toggle',
						name = 'Auto Greed',
						desc = 'Automatically greed uncommon (green) quality items at max level',
					},
					Disenchant = {
						order = 3,
						type = 'toggle',
						name = 'Auto Disenchant',
						desc = 'Automatically disenchant uncommon (green) quality items at max level'
					},
					ByLevel = {
						order = 4,
						type = 'toggle',
						name = 'Auto-roll based on a given level',
						desc = 'This will auto-roll if you are above the given level if: You cannot equip the item being rolled on, or the ilevel of your equipped item is higher than the item being rolled on or you have an heirloom equipped in that slot'
					},
					Level = {
						order = 5,
						type = 'range',
						name = 'Level to start auto-rolling from',
						min = 1, max = GetMaxPlayerLevel(), step = 1,
						disabled = function() return not LootConfirmOptions['ByLevel'] end,
					},
				},
			},
		},
	}
end

function LC:HandleEvent(event, ...)
	if not LootConfirmOptions['Confirm'] then return end
	if event == 'CONFIRM_LOOT_ROLL' or event == 'CONFIRM_DISENCHANT_ROLL' then
		local arg1, arg2 = ...
		ConfirmLootRoll(arg1, arg2)
	elseif event == 'LOOT_OPENED' or event == 'LOOT_BIND_CONFIRM' then
		local count = GetNumLootItems()
		if count == 0 then CloseLoot() return end
		for slot = 1, count do
			ConfirmLootSlot(slot)
		end
	end
end

-- LOOT_ROLL_TYPE_PASS, LOOT_ROLL_TYPE_NEED
local PlayerLevel, MaxPlayerLevel

function LC:START_LOOT_ROLL(event, id)
	if not (LootConfirmOptions['Greed'] or LootConfirmOptions['Disenchant']) then return end
	local Texture, Name, Count, Quality, BindOnPickUp, Need, Greed, Disenchant = GetLootRollItemInfo(id)
	local Link = GetLootRollItemLink(id)
	local ItemID = tonumber(strmatch(Link, 'item:(%d+)'))

	if ItemID == 43102 or ItemID == 52078 then
		RollOnLoot(id, LOOT_ROLL_TYPE_GREED)
	end

	if IsXPUserDisabled() then MaxPlayerLevel = PlayerLevel end
	if PlayerLevel ~= MaxPlayerLevel or (LootConfirmOptions['ByLevel'] and PlayerLevel < LootConfirmOptions['Level']) then return end
	if LootConfirmOptions['ByLevel'] then
		if IsEquippableItem(Name) then
			local _, _, _, ItemLevel, _, _, _, _, Slot = GetItemInfo(Name)
			local ItemLink = GetInventoryItemLink('player', Slot)
			local MatchItemLevel = select(4, GetItemInfo(ItemLink))
			if Quality ~= 7 and MatchItemLevel < ItemLevel then return end
		end
	end

	if Quality == ITEM_QUALITY_UNCOMMON then
		if LootConfirmOptions['Disenchant'] and Disenchant then
			RollOnLoot(id, LOOT_ROLL_TYPE_DISENCHANT)
		else
			RollOnLoot(id, LOOT_ROLL_TYPE_GREED)
		end
	end
end

function LC:PLAYER_LEVEL_UP(event, level)
	PlayerLevel = level
end

function LC:Initialize()
	MaxPlayerLevel = GetMaxPlayerLevel()
	PlayerLevel = UnitLevel('player')

	if IsAddOnLoaded('ElvUI') then
		E = ElvUI[1]
		if E.db.general then E.db.general.autoRoll = false end
	end

	if not LootConfirmOptions then LootConfirmOptions = {} end
	if LootConfirmOptions['Confirm'] == nil then LootConfirmOptions['Confirm'] = true end
	if LootConfirmOptions['Greed'] == nil then LootConfirmOptions['Greed'] = false end
	if LootConfirmOptions['Disenchant'] == nil then LootConfirmOptions['Disenchant'] = false end
	if LootConfirmOptions['ByLevel'] == nil then LootConfirmOptions['ByLevel'] = false end
	if LootConfirmOptions['Level'] == nil then LootConfirmOptions['Level'] = MaxPlayerLevel end

	UIParent:UnregisterEvent('LOOT_BIND_CONFIRM')
	UIParent:UnregisterEvent('CONFIRM_DISENCHANT_ROLL')
	UIParent:UnregisterEvent('CONFIRM_LOOT_ROLL')

	self:RegisterEvent('PLAYER_LEVEL_UP')
	self:RegisterEvent('CONFIRM_DISENCHANT_ROLL', 'HandleEvent')
	self:RegisterEvent('CONFIRM_LOOT_ROLL', 'HandleEvent')
	self:RegisterEvent('LOOT_OPENED', 'HandleEvent')
	self:RegisterEvent('LOOT_BIND_CONFIRM', 'HandleEvent')
	if IsAddOnLoaded('ElvUI') then
		local M = E:GetModule('Misc')
		hooksecurefunc(M, 'START_LOOT_ROLL', function(self, event, id) LC:START_LOOT_ROLL(event, id) end)
	else
		self:RegisterEvent('START_LOOT_ROLL')
	end
	EP:RegisterPlugin(AddOnName, LC.GetOptions)
end

LC:Initialize()